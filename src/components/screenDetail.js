import React, { useState, useEffect } from 'react';
import { Layout } from 'antd';
import { Button } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import { Image,  Row, Col } from 'antd';
import Movie from './movie'
const axios = require('axios');

const key = "b45f2d682f4b779f38392fa2ba56278b"
function ScreenDetail(props) {
    const { Header, Footer, Sider, Content } = Layout;
    const [person, setPersons] = useState(null);
    const [movies, setMovies] = useState([]);
    let history = useHistory();
    let { nombre } = useParams();
    // De forma similar a componentDidMount y componentDidUpdate
    useEffect(() => {
        // Actualiza el título del documento usando la API del navegador
        axios.get('https://api.themoviedb.org/3/search/person?api_key='+key+'&language=en-US&query='+nombre+'&page=1&include_adult=false')
        .then(function (response) {
          // handle success
          setPersons(response.data)
          return axios.get("https://api.themoviedb.org/3/person/"+response.data?.results[0].id+"/movie_credits?api_key="+key+"&language=en-US")
        }).then( (response)=>{
            setMovies(response.data.cast)
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    },[]);
    const lista = movies.map((formulario) => <Movie item={formulario} /> )
    return (
        <>
            <Layout>
                <Header>    <Button onClick={()=> history.push('/')} type="primary"> regresar </Button>    </Header>
                <Content>
                    <Row>
                        <Col span={6}>
                            {person!==null && <dvi>
                                                    <Image maxWidth={400}
                                                        src={"https://image.tmdb.org/t/p/w200/"+person?.results[0]?.profile_path}
                                                    />
                                                    <b><h2>{person?.results[0]?.name}</h2></b>
                                                    <p>{person?.results[0]?.gender==1?"female":"male" }</p>
                                                    <p>popularity: {person?.results[0]?.popularity}</p>
                                                </dvi>
                            }
                        </Col>
                        <Col span={18} >
                            <h1>Peliculas:</h1>
                            {lista.length>0?lista: "sin ningun resultado encontrado"}
                        </Col>
                    </Row>
                </Content>
            </Layout>
        </>
    )
}
export default ScreenDetail

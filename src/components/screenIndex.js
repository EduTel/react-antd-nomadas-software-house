import React, { useState, useEffect } from 'react';

import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";


const axios = require('axios');

const { Dragger } = Upload;



function ScreenIndex({ route, navigation }) {
    const history = useHistory();

    const props = {
        accept: ".jpg, .png",
        name: 'file',
        headers: {"Nomada": " ZGQ2NjI5YTEtNzM0Yy00M2E2LTk2ODItMzY0ZmYxYzgwYzk2"},
        action: 'https://whois.nomada.cloud/upload',
        multiple: false,
        onChange(info) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file.response?.actorName, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
                //navigation.navigate("detail")
                if( info.file.response?.error===""  ){
                    history.push("/detail/"+info.file.response?.actorName)
                }
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    return (
        <>
            <div id="container" style={{padding: 24 }}>
            </div>
                <Dragger {...props}>
                    <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">Click or drag file to this area to upload</p>
                    <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                    band files
                    </p>
                </Dragger>,
                {
                    document.getElementById('container')
                }
        </>
    )
}
export default ScreenIndex

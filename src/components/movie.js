import React, { useState, useEffect } from 'react';
import { Layout } from 'antd';
import { Button } from 'antd';
import { useHistory } from "react-router-dom";
import { Image,  Row, Col } from 'antd';

function Movie({item}) {
    const { Header, Footer, Sider, Content } = Layout;
    let history = useHistory();
    console.log(item)
    return (
        <>
            <div key={item.id}>
                <Row>
                    <Col span={20}>
                        <h3>{item.original_title}</h3>
                    </Col>
                    <Col span={4}>
                        <h2>{item.vote_average}/10</h2>
                    </Col>
                </Row>
                <Row>
                    <Col span={6}>
                        <Image src={"https://image.tmdb.org/t/p/w200/"+item.poster_path}
                        />
                    </Col>
                    <Col span={18} >
                        {item.overview}
                        <p><b>release date:</b> {item.release_date}</p>
                    </Col>
                </Row>
            </div>
        </>
    )
}
export default Movie

import logo from './logo.svg';
import './App.css';
import ScreenIndex  from "./components/screenIndex";
import ScreenDetail from "./components/screenDetail";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
    <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/detail/:nombre">
            <ScreenDetail/>
          </Route>
          <Route path="/">
            <ScreenIndex/>
          </Route>
        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;
